package org.gtr.web;

import java.sql.Connection;
import java.sql.DriverManager;

import org.junit.Test;

public class MySQLConnectionTest {

	private static final String DRIVER =
			"net.sf.log4jdbc.sql.jdbcapi.DriverSpy";
	private static final String URL =
			"jdbc:log4jdbc:mysql://192.168.1.68:3306/gtr";
	private static final String USER = 
			"gtr";
	private static final String PW =
			"gtr12#$";
	
	@Test
	public void testConnection() throws Exception {
		Class.forName(DRIVER);
		
		try(Connection con = DriverManager.getConnection(URL, USER, PW)){
			System.out.println(con);
		} catch(Exception e){
			e.printStackTrace();
		}
	}
}
