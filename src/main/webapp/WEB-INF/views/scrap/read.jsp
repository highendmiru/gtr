<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="../include/header.jsp"%>

<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">READ SCRAP</h3>
				</div>
				<!-- /.box-header -->

<form role="form" method="post">

	<input type='hidden' name='sno' value="${scrapVO.sno}">

</form>

<div class="box-body">
	<div class="form-group">
		<label for="exampleInputEmail1">Company</label> <input type="text"
			name='fromcompany' class="form-control" value="${scrapVO.fromcompany}"
			readonly="readonly">
	</div>
	<div class="form-group">
		<label for="exampleInputPassword1">Waybill</label>
		<textarea class="form-control" name="waybill"
			readonly="readonly">${scrapVO.waybill}</textarea>
	</div>
	<div class="form-group">
		<label for="exampleInputEmail1">Weight(KG)</label> <input type="text"
			name="kilogram" class="form-control" value="${scrapVO.kilogram}"
			readonly="readonly">
	</div>
</div>
<!-- /.box-body -->

<div class="box-footer">
	<button type="submit" class="btn btn-warning">Modify</button>
	<button type="submit" class="btn btn-danger">REMOVE</button>
	<button type="submit" class="btn btn-primary">LIST ALL</button>
</div>


				<script>
				
$(document).ready(function(){
	
	var formObj = $("form[role='form']");
	
	console.log(formObj);
	
	$(".btn-warning").on("click", function(){
		formObj.attr("action", "/scrap/modify");
		formObj.attr("method", "get");		
		formObj.submit();
	});
	
	$(".btn-danger").on("click", function(){
		formObj.attr("action", "/scrap/remove");
		formObj.submit();
	});
	
	$(".btn-primary").on("click", function(){
		self.location = "/scrap/listAll";
	});
	
});

</script>




			</div>
			<!-- /.box -->
		</div>
		<!--/.col (left) -->

	</div>
	<!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<%@include file="../include/footer.jsp"%>
