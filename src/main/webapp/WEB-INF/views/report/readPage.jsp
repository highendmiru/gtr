<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page session="true"%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
<%@include file="../include/header.jsp"%>

<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->


		<div class="col-md-12">
			<!-- general form elements -->
			<div class='box'>
				<div class="box-header with-border">
					<h3 class="box-title">MONTHLY SCRAP REPORT</h3>
				</div>
			</div>

			<div class="box">
				<div class="box-header with-border">
					<div style="width:300px">
						<canvas id="myChart" width="300" height="300"></canvas>
					</div>
				</div>
				</div>				
					<table class="table table-bordered">
						<tr>
							<th align=center>CLOSING MONTH</th>
							<th align=center>COMPANY</th>
							<th align=center>CLOSING AMOUNT(KG)</th>
							<th align=center>TYPE OF MATERIAL</th>
							<th align=center>WE BUY</th>
							<th align=center>BUYING PRICE</th>
							<th align=center>WE SELL</th>
							<th align=center>SELLING PRICE</th>
						</tr>

						<c:forEach items="${list}" var="reportVO">

							<tr>
								<td align=center><a
									href='/report/readPage${pageMaker.makeSearch(pageMaker.cri.page) }&closing_month=${reportVO.closing_month}'>${reportVO.closing_month}</a></td>
								<td align=center>${reportVO.company}</td>
								<td align=right><fmt:formatNumber value="${reportVO.closing_amount}" pattern="#,###"/></td>
								<td align=center>${reportVO.mat_type}</td>
								<td align=right>${reportVO.we_buy}</td>
								<td align=right><fmt:formatNumber value="${reportVO.we_buy*reportVO.closing_amount}" pattern="#,###.##"/> </td>
								<td align=right>${reportVO.we_sell }</td>
								<td align=right><fmt:formatNumber value="${reportVO.we_sell*reportVO.closing_amount}" pattern="#,###.##"/> </td>
							</tr>

						</c:forEach>

					</table>
				</div>
				<!-- /.box-body -->


				<div class="box-footer">
				</div>
				<!-- /.box-footer-->
			</div>
		</div>
		<!--/.col (left) -->

	</div>
	<!-- /.row -->
</section>
<!-- /.content -->


<script>
	var result = '${msg}';

	if (result == 'SUCCESS') {
		alert("처리가 완료되었습니다.");
	}
</script>

<script>
	$(document).ready(
			function() {

				$('#searchBtn').on(
						"click",
						function(event) {

							self.location = "list"
									+ '${pageMaker.makeQuery(1)}'
									+ "&searchType="
									+ $("select option:selected").val()
									+ "&keyword=" + $('#keywordInput').val();

						});

				$('#newBtn').on("click", function(evt) {

					self.location = "register";

				});

			});
</script>
<script>
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
<%@include file="../include/footer.jsp"%>
