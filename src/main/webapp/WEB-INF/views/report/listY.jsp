<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.time.YearMonth"%>
<%@page import="java.time.LocalDate"%>
<%@page import="java.time.format.DateTimeFormatter"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page session="true"%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
<%@include file="../include/header.jsp"%>

        <%
            Map<Integer, String> numberToString = new HashMap<Integer, String>();
					LocalDate a = LocalDate.now();
		            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM");
										
            		for(int i = 0; i < 300; i++) {
						LocalDate b = a.plusMonths(-i+1);
            			numberToString.put(i, formatter.format(b));
            		}

            pageContext.setAttribute("map", numberToString);
        %>


<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->


		<div class="col-md-12">
			<!-- general form elements -->
			<div class='box'>
				<div class="box-header with-border">
					<h3 class="box-title">MONTHLY SCRAP REPORT</h3>
				</div>
			</div>

			<div class="box">
				<div class="box-header with-border">
				<table>
					<tr>
						<td>
							<div style="width:300px">
								<canvas id="myChart" width="300" height="300"></canvas>
							</div>
						</td>
						<td>
							<div style="width:300px">
								<canvas id="myDoughnutChart" width="300" height="300"></canvas>
							</div>
						</td>
						<td>
							<div style="width:300px">
								<canvas id="myDoughnutChart2" width="300" height="300"></canvas>
							</div>
						</td>
					</tr>
				</table>
				</div>
				<script>
				var myMap = new Map();
				var myDmap = new Map();
				var mySmap = new Map();
				</script>
				</div>				
					<table class="table table-bordered table-striped table-hover">
						<tr style="background:#eaeaea;">
							<th class="text-center">CLOSING MONTH</th>
							<th class="text-center">COMPANY</th>
							<th class="text-center">TYPE OF MATERIAL</th>
							<th class="text-center">PERIOD</th>							
							<th class="text-center">CLOSING AMOUNT(KG)</th>
							<th class="text-center">WE BUY</th>
							<th class="text-center">BUYING PRICE</th>
							<th class="text-center">WE SELL</th>
							<th class="text-center">SELLING PRICE</th>
						</tr>
						<c:forEach items="${list}" var="reportVO">

							<tr>
								<td align=center>${reportVO.closing_month}</td>
								<td align=center>${reportVO.company}</td>
								<td align=center>${reportVO.mat_type}</td>
								<td align=center>${reportVO.period }</td>								
								<td align=right><fmt:formatNumber value="${reportVO.closing_amount}" pattern="#,###"/><c:set var="amount_sum" value="${amount_sum+reportVO.closing_amount}" /></td>
								<td align=right>${reportVO.we_buy}</td>
								<td align=right><fmt:formatNumber value="${reportVO.we_buy*reportVO.closing_amount}" pattern="#,###.##"/> <c:set var="buy_sum" value="${buy_sum+reportVO.we_buy*reportVO.closing_amount}" /></td>
								<td align=right>${reportVO.we_sell }</td>
								<td align=right><fmt:formatNumber value="${reportVO.we_sell*reportVO.closing_amount}" pattern="#,###.##"/> <c:set var="sell_sum" value="${sell_sum+reportVO.we_sell*reportVO.closing_amount}" /></td>
							</tr>
							<script>
								myMap.set("${reportVO.company},${reportVO.mat_type},Period ${reportVO.period }","${reportVO.closing_amount}");
								myDmap.set("${reportVO.company},${reportVO.mat_type},Period ${reportVO.period }",Number("${reportVO.we_buy*reportVO.closing_amount}").toFixed(2));
								mySmap.set("${reportVO.company},${reportVO.mat_type},Period ${reportVO.period }",Number("${reportVO.we_sell*reportVO.closing_amount}").toFixed(2));
							</script> 
						</c:forEach>
							<tr>
								<td align=center>TOTAL</td>
								<td align=center> - </td>
								<td align=center> - </td>
								<td align=center> - </td>
								<td align=right><fmt:formatNumber value="${amount_sum}" pattern="#,###"/></td>
								<td align=right>${reportVO.we_buy}</td>
								<td align=right><fmt:formatNumber value="${buy_sum}" pattern="#,###.##"/> </td>
								<td align=right>${reportVO.we_sell }</td>
								<td align=right><fmt:formatNumber value="${sell_sum}" pattern="#,###.##"/> </td>
							</tr>
					</table>
				</div>
				<!-- /.box-body -->

<script>

</script>

				<div class="box-footer">

					<div class="text-center">
					
						<c:forEach items="${getMonth}" var="reportVO">
						
						${reportVO.closing_month}
						
						</c:forEach>
					
						<ul class="pagination">
						
        <%-- JSTL foreach tag example to loop a HashMap in JSP --%>
<!--         <table>
            <c:forEach var="entry" items="${pageScope.map}">
                <tr><td><c:out value="${entry.key}"/></td> <td><c:out value="${entry.value}"/> </td></tr>
            </c:forEach>
        </table>  -->
        
							<c:if test="${rpageMaker.prev}">
								<li><a
									href="list${rpageMaker.makeSearch(rpageMaker.startPage - 1) }">&laquo;</a></li>
							</c:if>

							<c:forEach begin="${rpageMaker.startPage }"
								end="${rpageMaker.endPage }" var="entry" items="${pageScope.map}">
								<li
									<c:out value="${rpageMaker.cri.page == entry.key?'class =active':''}"/>>
									<a href="list${rpageMaker.makeSearch(entry.key)}">${entry.value}</a>
								</li>
							</c:forEach>

							<c:if test="${rpageMaker.next && rpageMaker.endPage > 0}">
								<li><a
									href="list${rpageMaker.makeSearch(rpageMaker.endPage +1) }">&raquo;</a></li>
							</c:if>

						</ul>
					</div>

				</div>
				<!-- /.box-footer-->
			</div>
		</div>
		<!--/.col (left) -->

	</div>
	<!-- /.row -->
</section>
<!-- /.content -->


<script>
	var result = '${msg}';

	if (result == 'SUCCESS') {
		alert("처리가 완료되었습니다.");
	}
</script>

<script>
	$(document).ready(
			function() {

				$('#searchBtn').on(
						"click",
						function(event) {

							self.location = "list"
									+ '${rpageMaker.makeQuery(1)}'
									+ "&searchType="
									+ $("select option:selected").val()
									+ "&keyword=" + $('#keywordInput').val();

						});

				$('#newBtn').on("click", function(evt) {

					self.location = "register";

				});
				
				for (var [key, value] of myMap) {
					  addData(myChart,key,value);
					}
				
				for (var [key, value] of myDmap) {
					  addData(myDoughnutChart,key,value);
					}

				for (var [key, value] of mySmap) {
					  addData(myDoughnutChart2,key,value);
					}
			});
	

	function addData(chart, label, data) {
	    chart.data.labels.push(label);
	    chart.data.datasets.forEach((dataset) => {
	        dataset.data.push(data);
	    });
	    chart.update();
	}
</script>
<script>
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [],
        datasets: [{
            data: [],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(181, 255, 64, 0.2)',
                'rgba(64, 235, 255, 0.2)',
                'rgba(255, 64, 226, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(181, 255, 64, 1)',
                'rgba(64, 235, 255, 1)',
                'rgba(255, 64, 226, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:false
                }
            }],
            xAxes:[{
                ticks: {
                	display:false
                }
            }]
        },
        barThickness:3,
    	title: {
        	display: true,
        	text: 'Total Closing Amount'
    	},
        legend: {
            display: false
        }
    }
});
</script>
<script>
var ctx = document.getElementById("myDoughnutChart").getContext('2d');
var myDoughnutChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: [],
        datasets: [{
            label: '# of Votes',
            data: [],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(181, 255, 64, 0.2)',
                'rgba(64, 235, 255, 0.2)',
                'rgba(255, 64, 226, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(64, 235, 255, 1)',
                'rgba(255, 64, 226, 1)'
            ],
            borderWidth: 1
        }]
    },
	options: {
		responsive: true,
    	title: {
        	display: true,
        	text: 'Total Buying'
    	},
        legend: {
            display: false,
        },
		animation: {
			animateScale: true,
			animateRotate: true
		},
	    tooltips: {
	        enabled: true
	    }
	}
});
</script>
<script>
var ctx = document.getElementById("myDoughnutChart2").getContext('2d');
var myDoughnutChart2 = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: [],
        datasets: [{
            label: '# of Votes',
            data: [],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(181, 255, 64, 0.2)',
                'rgba(64, 235, 255, 0.2)',
                'rgba(255, 64, 226, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(64, 235, 255, 1)',
                'rgba(255, 64, 226, 1)'
            ],
            borderWidth: 1
        }]
    },
	options: {
		responsive: true,
    	title: {
        	display: true,
        	text: 'Total Selling'
    	},
        legend: {
            display: false,
        },
		animation: {
			animateScale: true,
			animateRotate: true
		}
	}
});
</script>
<%@include file="../include/footer.jsp"%>
