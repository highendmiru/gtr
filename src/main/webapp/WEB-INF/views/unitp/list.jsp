<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page session="true"%>

<%@include file="../include/header.jsp"%>

<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->


		<div class="col-md-12">
			<!-- general form elements -->
			<div class='box'>
				<div class="box-header with-border">
					<h3 class="box-title">Scrap Unit Price List</h3>
				</div>


				<div class='box-body'>

					<select name="searchType">
						<option value="n"
							<c:out value="${cri.searchType == null?'selected':''}"/>>
							---</option>
						<option value="u"
							<c:out value="${cri.searchType eq 'u'?'selected':''}"/>>
							User</option>
						<option value="f"
							<c:out value="${cri.searchType eq 'f'?'selected':''}"/>>
							Company</option>
						<option value="w"
							<c:out value="${cri.searchType eq 'w'?'selected':''}"/>>
							Waybill</option>
						<option value="m"
							<c:out value="${cri.searchType eq 'm'?'selected':''}"/>>
							Type of Material</option>
						<option value="uf"
							<c:out value="${cri.searchType eq 'uf'?'selected':''}"/>>
							User OR Company</option>
						<option value="cw"
							<c:out value="${cri.searchType eq 'cw'?'selected':''}"/>>
							Company OR Waybill</option>
						<option value="cm"
							<c:out value="${cri.searchType eq 'cm'?'selected':''}"/>>
							Company OR Type Of Material</option>
						<option value="tcwm"
							<c:out value="${cri.searchType eq 'tcwm'?'selected':''}"/>>
							User OR Company OR Waybill OR Type of Material</option>
					</select> <input type="text" name='keyword' id="keywordInput"
						value='${cri.keyword }'>
					<button id='searchBtn'>Search</button>
					<button id='newBtn' style="float: right;">New Unit Price</button>

				</div>
			</div>


			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">LIST UNIT PRICE</h3>
				</div>
				<div class="box-body">
					<table class="table table-bordered">
						<tr>
							<th style="width: 10px">NO</th>
							<th>WRITER</th>
							<th>COMPANY</th>
							<th>TYPE OF MATERIAL</th>
							<th>UNIT PRICE</th>
							<th>WE BUY</th>
							<th>WE SELL</th>
							<th>CLOSING MONTH</th>
							<th>PERIOD</th>
							<th>REG DATE</th>
						</tr>

						<c:forEach items="${list}" var="unitpVO">

							<tr>
								<td><a
									href='/unitp/readPage${pageMaker.makeSearch(pageMaker.cri.page) }&uno=${unitpVO.uno}'>${unitpVO.uno}</a></td>
								<td>${unitpVO.userid} </a></td>
								<td>${unitpVO.company}</td>
								<td>${unitpVO.mat_type}</td>
								<td>${unitpVO.unit_price}</td>
								<td>${unitpVO.we_buy}</td>
								<td>${unitpVO.we_sell}</td>
								<td>${unitpVO.closing_month}</td>
								<td>${unitpVO.period}</td>
								<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm"
										value="${unitpVO.regdate}" /></td>
							</tr>

						</c:forEach>

					</table>
				</div>
				<!-- /.box-body -->


				<div class="box-footer">

					<div class="text-center">
						<ul class="pagination">

							<c:if test="${pageMaker.prev}">
								<li><a
									href="list${pageMaker.makeSearch(pageMaker.startPage - 1) }">&laquo;</a></li>
							</c:if>

							<c:forEach begin="${pageMaker.startPage }"
								end="${pageMaker.endPage }" var="idx">
								<li
									<c:out value="${pageMaker.cri.page == idx?'class =active':''}"/>>
									<a href="list${pageMaker.makeSearch(idx)}">${idx}</a>
								</li>
							</c:forEach>

							<c:if test="${pageMaker.next && pageMaker.endPage > 0}">
								<li><a
									href="list${pageMaker.makeSearch(pageMaker.endPage +1) }">&raquo;</a></li>
							</c:if>

						</ul>
					</div>

				</div>
				<!-- /.box-footer-->
			</div>
		</div>
		<!--/.col (left) -->

	</div>
	<!-- /.row -->
</section>
<!-- /.content -->


<script>
	var result = '${msg}';

	if (result == 'SUCCESS') {
		alert("처리가 완료되었습니다.");
	}
</script>

<script>
	$(document).ready(
			function() {

				$('#searchBtn').on(
						"click",
						function(event) {

							self.location = "list"
									+ '${pageMaker.makeQuery(1)}'
									+ "&searchType="
									+ $("select option:selected").val()
									+ "&keyword=" + $('#keywordInput').val();

						});

				$('#newBtn').on("click", function(evt) {

					self.location = "register";

				});

			});
</script>

<%@include file="../include/footer.jsp"%>
