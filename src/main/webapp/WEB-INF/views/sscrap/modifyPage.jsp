<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="../include/header.jsp"%>

<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">MODIFY BOARD</h3>
				</div>
				<!-- /.box-header -->

<form role="form" action="modifyPage" method="post">

	<input type='hidden' name='page' value="${cri.page}"> <input
		type='hidden' name='perPageNum' value="${cri.perPageNum}">
	<input type='hidden' name='searchType' value="${cri.searchType}">
	<input type='hidden' name='keyword' value="${cri.keyword}">

					<div class="box-body">

						<div class="form-group">
							<label for="exampleInputEmail1">SNO</label> <input type="text"
								name='sno' class="form-control" value="${scrapVO.sno}"
								readonly="readonly">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Writer</label> <input type="text"
								name="userid" class="form-control" value="${scrapVO.userid}"
								readonly="readonly">
						</div>

						<div class="form-group">
							<label for="exampleInputEmail1">Company</label><br/>
								<select name="fromcompany">
									<option value="HYUNDAI">HYUNDAI</option>
									<option value="HOWON">HOWON</option>
									<option value="GEFEST">GEFEST</option>
									<option value="STEEL">STEEL</option>
								</select> 
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Waybill</label>
							<input type="text" class="form-control" name="waybill" value="${scrapVO.waybill}">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Waybill Date</label>
							<input type="date" class="form-control" name="waybill_dt2" value="${scrapVO.waybill_dt2}">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Type of Material</label><br/>
								<select name="mat_type">
									<option value="PRESS SCRAP">PRESS SCRAP</option>
									<option value="STEEL SCRAP">STEEL SCRAP</option>
								</select> 
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Period</label><br/>
								<select name="period">
									<option value="1">Period 1</option>
									<option value="2">Period 2</option>
								</select> 
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Weight(KG)</label> <input
								type="text" name="kilogram" class="form-control"
								value="${scrapVO.kilogram}">
						</div>
					</div>
					<!-- /.box-body -->
				</form>
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">SAVE</button>
					<button type="submit" class="btn btn-warning">CANCEL</button>
				</div>

<script>
$(document).ready(
	function() {

		var formObj = $("form[role='form']");

		console.log(formObj);

		$(".btn-warning")
				.on("click",function() {
					self.location = "/sscrap/list?page=${cri.page}&perPageNum=${cri.perPageNum}"
							+ "&searchType=${cri.searchType}&keyword=${cri.keyword}";
				});

		$(".btn-primary").on("click",
				function() {
					formObj.submit();
				});
	});
</script>




			</div>
			<!-- /.box -->
		</div>
		<!--/.col (left) -->

	</div>
	<!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<%@include file="../include/footer.jsp"%>
