package org.gtr.persistence;

import java.util.List;

import org.gtr.domain.ScrapVO;
import org.gtr.domain.Criteria;
import org.gtr.domain.SearchCriteria;

public interface ScrapDAO {

  public void create(ScrapVO so) throws Exception;

  public ScrapVO read(Integer sno) throws Exception;

  public void update(ScrapVO so) throws Exception;

  public void delete(Integer sno) throws Exception;

  public List<ScrapVO> listAll() throws Exception;

  public List<ScrapVO> listPage(int page) throws Exception;

  public List<ScrapVO> listCriteria(Criteria cri) throws Exception;

  public int countPaging(Criteria cri) throws Exception;
  
  //use for dynamic sql
  
  public List<ScrapVO> listSearch(SearchCriteria cri)throws Exception;
  
  public int listSearchCount(SearchCriteria cri)throws Exception;
  
  public void addAttach(String fullName)throws Exception;
  
  public List<String> getAttach(Integer sno)throws Exception;  
   
  public void deleteAttach(Integer sno)throws Exception;
  
  public void replaceAttach(String fullName, Integer sno)throws Exception;

}
