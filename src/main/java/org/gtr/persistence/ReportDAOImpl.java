package org.gtr.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;
import org.gtr.domain.ReportVO;
import org.gtr.domain.Criteria;
import org.gtr.domain.SearchCriteria;
import org.gtr.domain.RSearchCriteria;

@Repository
public class ReportDAOImpl implements ReportDAO {

  @Inject
  private SqlSession session;

  private static String namespace = "org.gtr.mapper.ReportMapper";

  @Override
  public void create(ReportVO so) throws Exception {
    session.insert(namespace + ".create", so);
  }

  //@Override
  //public ReportVO read(Integer reno) throws Exception {
  // return session.selectOne(namespace + ".read", reno);
  //}
  
  
  @Override
  public List<ReportVO> read(String closing_month) throws Exception {
    return session.selectList(namespace + ".read", closing_month);
  }

  @Override
  public void update(ReportVO so) throws Exception {
    session.update(namespace + ".update", so);
  }

  @Override
  public void delete(Integer reno) throws Exception {
    session.delete(namespace + ".delete", reno);
  }

  @Override
  public List<ReportVO> listAll() throws Exception {
    return session.selectList(namespace + ".listAll");
  }

  @Override
  public List<ReportVO> listPage(int page) throws Exception {

    if (page <= 0) {
      page = 1;
    }

    page = (page - 1) * 10;

    return session.selectList(namespace + ".listPage", page);
  }

  @Override
  public List<ReportVO> listCriteria(Criteria cri) throws Exception {

    return session.selectList(namespace + ".listCriteria", cri);
  }

  @Override
  public int countPaging(Criteria cri) throws Exception {

    return session.selectOne(namespace + ".countPaging", cri);
  }

  @Override
  public List<ReportVO> listSearch(SearchCriteria cri) throws Exception {

    return session.selectList(namespace + ".listSearch", cri);
  }

  @Override
  public int listSearchCount(SearchCriteria cri) throws Exception {

    return session.selectOne(namespace + ".listSearchCount", cri);
  }
  
  @Override
  public List<ReportVO> RlistSearch(RSearchCriteria cri) throws Exception {

    return session.selectList(namespace + ".RlistSearch", cri);
  }

  @Override
  public int RlistSearchCount(RSearchCriteria cri) throws Exception {

    return session.selectOne(namespace + ".RlistSearchCount", cri);
  }
  
  @Override
  public void addAttach(String fullName) throws Exception {
    
    session.insert(namespace+".addAttach", fullName);
    
  }
  
  @Override
  public List<String> getAttach(Integer reno) throws Exception {
    
    return session.selectList(namespace +".getAttach", reno);
  }
 
  @Override
  public void deleteAttach(Integer reno) throws Exception {

    session.delete(namespace+".deleteAttach", reno);
    
  }

  @Override
  public void replaceAttach(String fullName, Integer reno) throws Exception {
    
    Map<String, Object> paramMap = new HashMap<String, Object>();
    
    paramMap.put("reno", reno);
    paramMap.put("fullName", fullName);
    
    session.insert(namespace+".replaceAttach", paramMap);
    
  }

}
