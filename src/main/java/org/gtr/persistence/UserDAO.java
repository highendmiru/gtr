package org.gtr.persistence;

import java.util.Date;

import org.gtr.domain.UserVO;
import org.gtr.dto.LoginDTO;

public interface UserDAO {

	public UserVO login(LoginDTO dto)throws Exception;

  public void keepLogin(String uid, String sessionId, Date next);
  
  public UserVO checkUserWithSessionKey(String value);	
}



