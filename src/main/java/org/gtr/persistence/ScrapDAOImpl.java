package org.gtr.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;
import org.gtr.domain.ScrapVO;
import org.gtr.domain.Criteria;
import org.gtr.domain.SearchCriteria;

@Repository
public class ScrapDAOImpl implements ScrapDAO {

  @Inject
  private SqlSession session;

  private static String namespace = "org.gtr.mapper.ScrapMapper";

  @Override
  public void create(ScrapVO so) throws Exception {
    session.insert(namespace + ".create", so);
  }

  @Override
  public ScrapVO read(Integer sno) throws Exception {
    return session.selectOne(namespace + ".read", sno);
  }

  @Override
  public void update(ScrapVO so) throws Exception {
    session.update(namespace + ".update", so);
  }

  @Override
  public void delete(Integer sno) throws Exception {
    session.delete(namespace + ".delete", sno);
  }

  @Override
  public List<ScrapVO> listAll() throws Exception {
    return session.selectList(namespace + ".listAll");
  }

  @Override
  public List<ScrapVO> listPage(int page) throws Exception {

    if (page <= 0) {
      page = 1;
    }

    page = (page - 1) * 10;

    return session.selectList(namespace + ".listPage", page);
  }

  @Override
  public List<ScrapVO> listCriteria(Criteria cri) throws Exception {

    return session.selectList(namespace + ".listCriteria", cri);
  }

  @Override
  public int countPaging(Criteria cri) throws Exception {

    return session.selectOne(namespace + ".countPaging", cri);
  }

  @Override
  public List<ScrapVO> listSearch(SearchCriteria cri) throws Exception {

    return session.selectList(namespace + ".listSearch", cri);
  }

  @Override
  public int listSearchCount(SearchCriteria cri) throws Exception {

    return session.selectOne(namespace + ".listSearchCount", cri);
  }
  @Override
  public void addAttach(String fullName) throws Exception {
    
    session.insert(namespace+".addAttach", fullName);
    
  }
  
  @Override
  public List<String> getAttach(Integer sno) throws Exception {
    
    return session.selectList(namespace +".getAttach", sno);
  }
 

  @Override
  public void deleteAttach(Integer sno) throws Exception {

    session.delete(namespace+".deleteAttach", sno);
    
  }

  @Override
  public void replaceAttach(String fullName, Integer sno) throws Exception {
    
    Map<String, Object> paramMap = new HashMap<String, Object>();
    
    paramMap.put("sno", sno);
    paramMap.put("fullName", fullName);
    
    session.insert(namespace+".replaceAttach", paramMap);
    
  }

}
