package org.gtr.persistence;

import java.util.List;

import org.gtr.domain.ReportVO;
import org.gtr.domain.Criteria;
import org.gtr.domain.SearchCriteria;
import org.gtr.domain.RSearchCriteria;

public interface ReportDAO {

  public void create(ReportVO so) throws Exception;

//  public ReportVO read(Integer reno) throws Exception;
  
  public List<ReportVO> read(String closing_month) throws Exception;

  public void update(ReportVO so) throws Exception;

  public void delete(Integer reno) throws Exception;

  public List<ReportVO> listAll() throws Exception;

  public List<ReportVO> listPage(int page) throws Exception;

  public List<ReportVO> listCriteria(Criteria cri) throws Exception;

  public int countPaging(Criteria cri) throws Exception;
  
  //use for dynamic sql
  
  public List<ReportVO> listSearch(SearchCriteria cri)throws Exception;
  
  public int listSearchCount(SearchCriteria cri)throws Exception;
  
  public List<ReportVO> RlistSearch(RSearchCriteria cri)throws Exception;
  
  public int RlistSearchCount(RSearchCriteria cri)throws Exception;
  
  public void addAttach(String fullName)throws Exception;
  
  public List<String> getAttach(Integer reno)throws Exception;  
  
  public void deleteAttach(Integer reno)throws Exception;
  
  public void replaceAttach(String fullName, Integer reno)throws Exception;

}
