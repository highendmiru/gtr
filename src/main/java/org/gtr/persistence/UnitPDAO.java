package org.gtr.persistence;

import java.util.List;

import org.gtr.domain.UnitPVO;
import org.gtr.domain.Criteria;
import org.gtr.domain.SearchCriteria;

public interface UnitPDAO {

  public void create(UnitPVO so) throws Exception;

  public UnitPVO read(Integer uno) throws Exception;

  public void update(UnitPVO so) throws Exception;

  public void delete(Integer uno) throws Exception;

  public List<UnitPVO> listAll() throws Exception;

  public List<UnitPVO> listPage(int page) throws Exception;

  public List<UnitPVO> listCriteria(Criteria cri) throws Exception;

  public int countPaging(Criteria cri) throws Exception;
  
  //use for dynamic sql
  
  public List<UnitPVO> listSearch(SearchCriteria cri)throws Exception;
  
  public int listSearchCount(SearchCriteria cri)throws Exception;
  
  public void addAttach(String fullName)throws Exception;
  
  public List<String> getAttach(Integer uno)throws Exception;  
   
  public void deleteAttach(Integer uno)throws Exception;
  
  public void replaceAttach(String fullName, Integer uno)throws Exception;

}
