package org.gtr.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;
import org.gtr.domain.UnitPVO;
import org.gtr.domain.Criteria;
import org.gtr.domain.SearchCriteria;

@Repository
public class UnitPDAOImpl implements UnitPDAO {

  @Inject
  private SqlSession session;

  private static String namespace = "org.gtr.mapper.UnitPMapper";

  @Override
  public void create(UnitPVO so) throws Exception {
    session.insert(namespace + ".create", so);
  }

  @Override
  public UnitPVO read(Integer uno) throws Exception {
    return session.selectOne(namespace + ".read", uno);
  }

  @Override
  public void update(UnitPVO so) throws Exception {
    session.update(namespace + ".update", so);
  }

  @Override
  public void delete(Integer uno) throws Exception {
    session.delete(namespace + ".delete", uno);
  }

  @Override
  public List<UnitPVO> listAll() throws Exception {
    return session.selectList(namespace + ".listAll");
  }

  @Override
  public List<UnitPVO> listPage(int page) throws Exception {

    if (page <= 0) {
      page = 1;
    }

    page = (page - 1) * 10;

    return session.selectList(namespace + ".listPage", page);
  }

  @Override
  public List<UnitPVO> listCriteria(Criteria cri) throws Exception {

    return session.selectList(namespace + ".listCriteria", cri);
  }

  @Override
  public int countPaging(Criteria cri) throws Exception {

    return session.selectOne(namespace + ".countPaging", cri);
  }

  @Override
  public List<UnitPVO> listSearch(SearchCriteria cri) throws Exception {

    return session.selectList(namespace + ".listSearch", cri);
  }

  @Override
  public int listSearchCount(SearchCriteria cri) throws Exception {

    return session.selectOne(namespace + ".listSearchCount", cri);
  }
  @Override
  public void addAttach(String fullName) throws Exception {
    
    session.insert(namespace+".addAttach", fullName);
    
  }
  
  @Override
  public List<String> getAttach(Integer uno) throws Exception {
    
    return session.selectList(namespace +".getAttach", uno);
  }
 

  @Override
  public void deleteAttach(Integer uno) throws Exception {

    session.delete(namespace+".deleteAttach", uno);
    
  }

  @Override
  public void replaceAttach(String fullName, Integer uno) throws Exception {
    
    Map<String, Object> paramMap = new HashMap<String, Object>();
    
    paramMap.put("sno", uno);
    paramMap.put("fullName", fullName);
    
    session.insert(namespace+".replaceAttach", paramMap);
    
  }

}
