package org.gtr.controller;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.gtr.domain.ReportVO;
import org.gtr.domain.PageMaker;
import org.gtr.domain.RPageMaker;
import org.gtr.domain.SearchCriteria;
import org.gtr.domain.RSearchCriteria;
import org.gtr.service.ReportService;

@Controller
@RequestMapping("/report/*")
public class ReportController {

  private static final Logger logger = LoggerFactory.getLogger(ReportController.class);

  @Inject
  private ReportService service;

  @RequestMapping(value = "/list", method = RequestMethod.GET)
  public void listPage(@ModelAttribute("cri") RSearchCriteria cri, Model model) throws Exception {

    logger.info(cri.toString());

    // model.addAttribute("list", service.listCriteria(cri));
    model.addAttribute("list", service.RlistSearchCriteria(cri));
    
    RPageMaker rpageMaker = new RPageMaker();
    rpageMaker.setCri(cri);

    // pageMaker.setTotalCount(service.listCountCriteria(cri));
    rpageMaker.setTotalCount(service.RlistSearchCount(cri));

    model.addAttribute("rpageMaker", rpageMaker);
  }

  @RequestMapping(value = "/listY", method = RequestMethod.GET)
  public void listPageY(@ModelAttribute("cri") RSearchCriteria cri, Model model) throws Exception {

    logger.info(cri.toString());

    // model.addAttribute("list", service.listCriteria(cri));
    model.addAttribute("list", service.RlistSearchCriteria(cri));
    
    RPageMaker rpageMaker = new RPageMaker();
    rpageMaker.setCri(cri);

    // pageMaker.setTotalCount(service.listCountCriteria(cri));
    rpageMaker.setTotalCount(service.RlistSearchCount(cri));

    model.addAttribute("rpageMaker", rpageMaker);
  }  
  
  @RequestMapping(value = "/readPage", method = RequestMethod.GET)
  public void read(@RequestParam("closing_month") String closing_month, @ModelAttribute("cri") SearchCriteria cri, Model model)
      throws Exception {
	
    model.addAttribute("list",service.read(closing_month));
    
  }
  
  /*
  @RequestMapping(value = "/readPage", method = RequestMethod.GET)
  public void read(@RequestParam("reno") int reno, @ModelAttribute("cri") SearchCriteria cri, Model model)
      throws Exception {

    model.addAttribute("reportVO",service.read(reno));
  }

  @RequestMapping(value = "/removePage", method = RequestMethod.POST)
  public String remove(@RequestParam("reno") int reno, SearchCriteria cri, RedirectAttributes rttr) throws Exception {

    service.remove(reno);

    rttr.addAttribute("page", cri.getPage());
    rttr.addAttribute("perPageNum", cri.getPerPageNum());
    rttr.addAttribute("searchType", cri.getSearchType());
    rttr.addAttribute("keyword", cri.getKeyword());

    rttr.addFlashAttribute("msg", "SUCCESS");

    return "redirect:/report/list";
  }

  @RequestMapping(value = "/modifyPage", method = RequestMethod.GET)
  public void modifyPagingGET(int reno, @ModelAttribute("cri") SearchCriteria cri, Model model) throws Exception {

    model.addAttribute("reportVO",service.read(reno));
  }

  @RequestMapping(value = "/modifyPage", method = RequestMethod.POST)
  public String modifyPagingPOST(ReportVO report, SearchCriteria cri, RedirectAttributes rttr) throws Exception {

    logger.info(cri.toString());
    service.modify(report);

    rttr.addAttribute("page", cri.getPage());
    rttr.addAttribute("perPageNum", cri.getPerPageNum());
    rttr.addAttribute("searchType", cri.getSearchType());
    rttr.addAttribute("keyword", cri.getKeyword());

    rttr.addFlashAttribute("msg", "SUCCESS");

    logger.info(rttr.toString());

    return "redirect:/report/list";
  }

  @RequestMapping(value = "/register", method = RequestMethod.GET)
  public void registGET() throws Exception {

    logger.info("regist get ...........");
  }

  @RequestMapping(value = "/register", method = RequestMethod.POST)
  public String registPOST(ReportVO board, RedirectAttributes rttr) throws Exception {

    logger.info("regist post ...........");
    logger.info(board.toString());

    service.regist(board);

    rttr.addFlashAttribute("msg", "SUCCESS");

    return "redirect:/report/list";
  } */

  // @RequestMapping(value = "/list", method = RequestMethod.GET)
  // public void listPage(@ModelAttribute("cri") SearchCriteria cri,
  // Model model) throws Exception {
  //
  // logger.info(cri.toString());
  //
  // model.addAttribute("list", service.listCriteria(cri));
  //
  // PageMaker pageMaker = new PageMaker();
  // pageMaker.setCri(cri);
  //
  // pageMaker.setTotalCount(service.listCountCriteria(cri));
  //
  // model.addAttribute("pageMaker", pageMaker);
  // }
}
