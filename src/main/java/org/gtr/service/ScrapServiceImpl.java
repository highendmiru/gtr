package org.gtr.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.gtr.domain.ScrapVO;
import org.gtr.domain.Criteria;
import org.gtr.domain.SearchCriteria;
import org.gtr.persistence.ScrapDAO;

@Service
public class ScrapServiceImpl implements ScrapService {

  @Inject
  private ScrapDAO dao;

  @Override
  public void regist(ScrapVO scrap) throws Exception {
    dao.create(scrap);
    
    String[] files = scrap.getFiles();
    
    if(files == null) { return; } 
    
    for (String fileName : files) {
    	dao.addAttach(fileName);
    }
  }

  @Override
  public ScrapVO read(Integer sno) throws Exception {
    return dao.read(sno);
  }

  @Override
  public void modify(ScrapVO scrap) throws Exception {
    dao.update(scrap);
    
    Integer sno = scrap.getSno();
    
    dao.deleteAttach(sno);
    
    String[] files = scrap.getFiles();
    
    if(files == null) { return; } 
    
    for (String fileName : files) {
      dao.replaceAttach(fileName, sno);
    }
  }

  @Override
  public void remove(Integer sno) throws Exception {
    dao.delete(sno);
  }

  @Override
  public List<ScrapVO> listAll() throws Exception {
    return dao.listAll();
  }

  @Override
  public List<ScrapVO> listCriteria(Criteria cri) throws Exception {

    return dao.listCriteria(cri);
  }

  @Override
  public int listCountCriteria(Criteria cri) throws Exception {

    return dao.countPaging(cri);
  }

  @Override
  public List<ScrapVO> listSearchCriteria(SearchCriteria cri) throws Exception {

    return dao.listSearch(cri);
  }

  @Override
  public int listSearchCount(SearchCriteria cri) throws Exception {

    return dao.listSearchCount(cri);
  }
  
  @Override
  public List<String> getAttach(Integer sno) throws Exception {
    
    return dao.getAttach(sno);
  }   

}
