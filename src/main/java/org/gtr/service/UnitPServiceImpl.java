package org.gtr.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.gtr.domain.UnitPVO;
import org.gtr.domain.Criteria;
import org.gtr.domain.SearchCriteria;
import org.gtr.persistence.UnitPDAO;

@Service
public class UnitPServiceImpl implements UnitPService {

  @Inject
  private UnitPDAO dao;

  @Override
  public void regist(UnitPVO unitp) throws Exception {
    dao.create(unitp);
    
    String[] files = unitp.getFiles();
    
    if(files == null) { return; } 
    
    for (String fileName : files) {
    	dao.addAttach(fileName);
    }
  }

  @Override
  public UnitPVO read(Integer uno) throws Exception {
    return dao.read(uno);
  }

  @Override
  public void modify(UnitPVO unitp) throws Exception {
    dao.update(unitp);
    
    Integer uno = unitp.getUno();
    
    dao.deleteAttach(uno);
    
    String[] files = unitp.getFiles();
    
    if(files == null) { return; } 
    
    for (String fileName : files) {
      dao.replaceAttach(fileName, uno);
    }
  }

  @Override
  public void remove(Integer uno) throws Exception {
    dao.delete(uno);
  }

  @Override
  public List<UnitPVO> listAll() throws Exception {
    return dao.listAll();
  }

  @Override
  public List<UnitPVO> listCriteria(Criteria cri) throws Exception {

    return dao.listCriteria(cri);
  }

  @Override
  public int listCountCriteria(Criteria cri) throws Exception {

    return dao.countPaging(cri);
  }

  @Override
  public List<UnitPVO> listSearchCriteria(SearchCriteria cri) throws Exception {

    return dao.listSearch(cri);
  }

  @Override
  public int listSearchCount(SearchCriteria cri) throws Exception {

    return dao.listSearchCount(cri);
  }
  
  @Override
  public List<String> getAttach(Integer uno) throws Exception {
    
    return dao.getAttach(uno);
  }   

}
