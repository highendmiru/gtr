package org.gtr.service;

import java.util.Date;

import org.gtr.domain.UserVO;
import org.gtr.dto.LoginDTO;

public interface UserService {

  public UserVO login(LoginDTO dto) throws Exception;

  public void keepLogin(String uid, String sessionId, Date next)throws Exception;
  
  public UserVO checkLoginBefore(String value);  
}
