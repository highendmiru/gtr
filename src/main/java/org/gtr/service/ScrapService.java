package org.gtr.service;

import java.util.List;

import org.gtr.domain.ScrapVO;
import org.gtr.domain.Criteria;
import org.gtr.domain.SearchCriteria;

public interface ScrapService {

  public void regist(ScrapVO scrap) throws Exception;

  public ScrapVO read(Integer sno) throws Exception;

  public void modify(ScrapVO scrap) throws Exception;

  public void remove(Integer sno) throws Exception;

  public List<ScrapVO> listAll() throws Exception;

  public List<ScrapVO> listCriteria(Criteria cri) throws Exception;

  public int listCountCriteria(Criteria cri) throws Exception;

  public List<ScrapVO> listSearchCriteria(SearchCriteria cri) 
      throws Exception;

  public int listSearchCount(SearchCriteria cri) throws Exception;
  
  public List<String> getAttach(Integer bno)throws Exception;
}
