package org.gtr.service;

import java.util.List;

import org.gtr.domain.ReportVO;
import org.gtr.domain.Criteria;
import org.gtr.domain.RCriteria;
import org.gtr.domain.SearchCriteria;
import org.gtr.domain.RSearchCriteria;

public interface ReportService {

  public void regist(ReportVO report) throws Exception;

  //public ReportVO read(Integer reno) throws Exception;
  
  public List<ReportVO> read(String closing_month) throws Exception;

  public void modify(ReportVO report) throws Exception;

  public void remove(Integer reno) throws Exception;

  public List<ReportVO> listAll() throws Exception;

  public List<ReportVO> listCriteria(Criteria cri) throws Exception;

  public int listCountCriteria(Criteria cri) throws Exception;

  public List<ReportVO> listSearchCriteria(SearchCriteria cri) 
      throws Exception;

  public int listSearchCount(SearchCriteria cri) throws Exception;
  
  public List<ReportVO> RlistSearchCriteria(RSearchCriteria cri) 
	      throws Exception;

  public int RlistSearchCount(RSearchCriteria cri) throws Exception;
  
  public List<String> getAttach(Integer reno)throws Exception;
  
}
