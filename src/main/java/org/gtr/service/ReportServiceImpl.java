package org.gtr.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.gtr.domain.ReportVO;
import org.gtr.domain.Criteria;
import org.gtr.domain.SearchCriteria;
import org.gtr.domain.RCriteria;
import org.gtr.domain.RSearchCriteria;
import org.gtr.persistence.ReportDAO;

@Service
public class ReportServiceImpl implements ReportService {

  @Inject
  private ReportDAO dao;

  @Override
  public void regist(ReportVO report) throws Exception {
    dao.create(report);
    
    String[] files = report.getFiles();
    
    if(files == null) { return; } 
    
    for (String fileName : files) {
    	dao.addAttach(fileName);
    }
  }

//  @Override
//  public ReportVO read(Integer reno) throws Exception {
//    return dao.read(reno);
//  }
  
  @Override
  public List<ReportVO> read(String closing_month) throws Exception {
    return dao.read(closing_month);
  }

  @Override
  public void modify(ReportVO report) throws Exception {
    dao.update(report);
    
    Integer reno = report.getUno();
    
    dao.deleteAttach(reno);
    
    String[] files = report.getFiles();
    
    if(files == null) { return; } 
    
    for (String fileName : files) {
      dao.replaceAttach(fileName, reno);
    }
  }

  @Override
  public void remove(Integer reno) throws Exception {
    dao.delete(reno);
  }

  @Override
  public List<ReportVO> listAll() throws Exception {
    return dao.listAll();
  }

  @Override
  public List<ReportVO> listCriteria(Criteria cri) throws Exception {

    return dao.listCriteria(cri);
  }

  @Override
  public int listCountCriteria(Criteria cri) throws Exception {

    return dao.countPaging(cri);
  }

  @Override
  public List<ReportVO> listSearchCriteria(SearchCriteria cri) throws Exception {

    return dao.listSearch(cri);
  }

  @Override
  public int listSearchCount(SearchCriteria cri) throws Exception {

    return dao.listSearchCount(cri);
  }
  
  @Override
  public List<ReportVO> RlistSearchCriteria(RSearchCriteria cri) throws Exception {

    return dao.RlistSearch(cri);
  }

  @Override
  public int RlistSearchCount(RSearchCriteria cri) throws Exception {

    return dao.RlistSearchCount(cri);
  }
  
  @Override
  public List<String> getAttach(Integer reno) throws Exception {
    
    return dao.getAttach(reno);
  }
  
}
