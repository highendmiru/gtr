package org.gtr.service;

import java.util.List;

import org.gtr.domain.UnitPVO;
import org.gtr.domain.Criteria;
import org.gtr.domain.SearchCriteria;

public interface UnitPService {

  public void regist(UnitPVO unitp) throws Exception;

  public UnitPVO read(Integer uno) throws Exception;

  public void modify(UnitPVO unitp) throws Exception;

  public void remove(Integer uno) throws Exception;

  public List<UnitPVO> listAll() throws Exception;

  public List<UnitPVO> listCriteria(Criteria cri) throws Exception;

  public int listCountCriteria(Criteria cri) throws Exception;

  public List<UnitPVO> listSearchCriteria(SearchCriteria cri) 
      throws Exception;

  public int listSearchCount(SearchCriteria cri) throws Exception;
  
  public List<String> getAttach(Integer uno)throws Exception;
}
