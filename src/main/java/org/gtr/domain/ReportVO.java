package org.gtr.domain;

import java.util.Arrays;
import java.util.Date;

public class ReportVO {
	
	private Integer reno;
	private String company;
	private String closing_month;
	private float closing_amount;
	private String mat_type;
	private int period;
	private Date regdate;
	private float we_buy;
	private float we_sell;
	
	private String[] files;
	
	public Integer getUno() {
		return reno;
	}
	public void setUno(Integer reno) {
		this.reno = reno;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getClosing_month() {
		return closing_month;
	}
	public void setClosing_month(String closing_month) {
		this.closing_month = closing_month;
	}
	public Float getClosing_amount() {
		return closing_amount;
	}
	public void setClosing_amount(Float closing_amount) {
		this.closing_amount = closing_amount;
	}
	public String getMat_type() {
		return mat_type;
	}
	public void setMat_type(String mat_type) {
		this.mat_type = mat_type;
	}
	public Integer getPeriod() {
		return period;
	}
	public void setPeriod(Integer period) {
		this.period = period;
	}
	public Float getWe_buy() {
		return we_buy;
	}
	public void setWe_buy(Float we_buy) {
		this.we_buy = we_buy;
	}
	public Float getWe_sell() {
		return we_sell;
	}
	public void setWe_sell(Float we_sell) {
		this.we_sell = we_sell;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public String[] getFiles() {
		return files;
	}
	public void setFiles(String[] flies) {
		this.files = files;
	}
	@Override
	public String toString() {
		return "ReportVO [reno=" + reno + ", company=" + company	
				+ ", closing_month=" + closing_month + ", closing_amount=" + closing_amount 
				+ ", mat_type=" + mat_type	+ ", period=" + period	+ ", regdate=" + regdate + ", files=" + Arrays.toString(files) +"]";
	}
}