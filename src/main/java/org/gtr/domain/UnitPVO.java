package org.gtr.domain;

import java.util.Arrays;
import java.util.Date;

public class UnitPVO {
	
	private Integer uno;
	private String userid;
	private String company;
	private String mat_type;
	private Float unit_price;
	private Float we_buy;
	private Float we_sell;
	private String closing_month;
	private int period;
	private Date regdate;
	
	private String[] files;
	
	public Integer getUno() {
		return uno;
	}
	public void setUno(Integer uno) {
		this.uno = uno;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getMat_type() {
		return mat_type;
	}
	public void setMat_type(String mat_type) {
		this.mat_type = mat_type;
	}	
	public Float getUnit_price() {
		return unit_price;
	}
	public void setUnit_price(Float unit_price) {
		this.unit_price = unit_price;
	}
	public Float getWe_buy() {
		return we_buy;
	}
	public void setWe_buy(Float we_buy) {
		this.we_buy = we_buy;
	}
	public Float getWe_sell() {
		return we_sell;
	}
	public String getClosing_month() {
		return closing_month;
	}
	public void setClosing_month(String closing_month) {
		this.closing_month = closing_month;
	}	
	public void setWe_sell(Float we_sell) {
		this.we_sell = we_sell;
	}
	public Integer getPeriod() {
		return period;
	}
	public void setPeriod(Integer period) {
		this.period = period;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public String[] getFiles() {
		return files;
	}
	public void setFiles(String[] flies) {
		this.files = files;
	}
	@Override
	public String toString() {
		return "UnitPVO [uno=" + uno + ", userid=" + userid + ", company=" + company 
				+ ", mat_type=" + mat_type + ", unit_price=" + unit_price + ", we_buy=" + we_buy
				+ ", we_sell=" + we_sell+ ", closing_month=" + closing_month + ", period=" + period
				+ ", regdate=" + regdate + ", files=" + Arrays.toString(files) +"]";
	}
}