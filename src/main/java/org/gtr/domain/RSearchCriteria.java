package org.gtr.domain;

public class RSearchCriteria extends RCriteria{

	private String searchType;
	private String keyword;
	private String waybill_fr;
	private String waybill_to;
	
	public String getSearchType() {
		return searchType;
	}
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getWaybill_fr() {
		return waybill_fr;
	}
	public void setWaybill_fr(String waybill_fr) {
		this.waybill_fr = waybill_fr;
	}
	public String getWaybill_to() {
		return waybill_to;
	}
	public void setWaybill_to(String waybill_to) {
		this.waybill_to = waybill_to;
	}
	@Override
	public String toString() {
		return super.toString() + " RSearchCriteria "
				+ "[searchType=" + searchType + ", keyword=" + keyword + ", waybill_fr=" + waybill_fr + ", waybill_to=" + waybill_to + "]";
	}
}


